<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:edi="http://www.navis.com/argo" targetNamespace="http://www.navis.com/argo"
           elementFormDefault="qualified" attributeFormDefault="qualified">
    <xs:include schemaLocation="baseMapping.xsd"/>
    <xs:element name="bookingTransactions">
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="edi:bookingTransaction" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="bookingTransaction">
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="edi:basetransaction">
                    <xs:sequence>                        
                        <xs:element name="ediVesselVisit" type="edi:ediVesselVisit" minOccurs="0" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>Vessel, voyage number and carrier details of a booking</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="loadFacility" type="edi:ediFacility" minOccurs="0" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>Used for Booking - refers to the facility at which the vessel actually calls.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="relayFacility" type="edi:ediFacility" minOccurs="0" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>Refers to facility which is used to pre-advise the unit in case EDI is for non-operational facility</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="dispatchFacility" type="edi:ediFacility" minOccurs="0" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>Used for EDO - refers to the facility from where the empty containers will be delivered.</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="ediTruckVisit" type="edi:ediTruckVisit" minOccurs="0"/>
                        <xs:element name="ediTrainVisit" type="edi:ediTrainVisit" minOccurs="0"/>
                        <xs:element name="ediBooking" type="edi:ediBooking" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>Booking Details such as booking number, export reference number, destination etc</xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="shipper" type="edi:shipper" minOccurs="0"/>
                        <xs:element name="consignee" type="edi:consignee" minOccurs="0"/>
                        <xs:element name="lineOperator" type="edi:ediOperator" minOccurs="0"/>
                        <xs:element name="shipperAgent" type="edi:shipperAgent" minOccurs="0"/>
                        <xs:element name="truckingCompany" type="edi:truckingCompany" minOccurs="0"/>
                        <xs:element name="ediHazard" type="edi:ediHazard" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Hazard details associated with the booking simply state that the containers arriving for
                                    this booking will contain these hazards. This does not mean that all containers for this booking will
                                    contain all of the hazard classes defined in the booking details.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="ediBookingType" type="xs:string" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>Export Booking or EDO should be one of the folllowing
                                    1. BOOKING - (Export Booking, default)
                                    2. EDO - (Equipment Delivery Order)
                                    3. BOOKING DISPATCH - (Empty Dispatch for export booking)
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="loadPort" type="edi:port" minOccurs="0"/>
                        <xs:element name="dischargePort1" type="edi:port" minOccurs="0"/>
                        <xs:element name="dischargePort2" type="edi:port" minOccurs="0"/>
                        <xs:element name="optionalPort1" type="edi:port" minOccurs="0"/>
                        <xs:element name="optionalPort2" type="edi:port" minOccurs="0"/>
                        <xs:element name="optionalPort3" type="edi:port" minOccurs="0"/>
                        <xs:element name="Origin" type="edi:port" minOccurs="0"/>
                        <xs:element name="Destination" type="edi:port" minOccurs="0"/>
                        <xs:element name="ownedOrLeasedOnly" type="xs:string" minOccurs="0">
                            <xs:annotation>
                                <xs:documentation>owned or leased may be one of the folllowing
                                    1. OWNED - (Owned equipment only)
                                    2. LEASED - (Leased equipment only)
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="dispatchReservedPriority" type="xs:string" minOccurs="0" maxOccurs="1">
                            <xs:annotation>
                                <xs:documentation>It may be one of the folllowing
                                    1. FIRST - (Dispatch reserved equipment FIRST)
                                    2. ONLY - (Dispatch reserved equipment ONLY)
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="specialStow" type="xs:string" minOccurs="0" maxOccurs="1"/>

                        <xs:element name="ediBookingItem" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Represents a booking item</xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="sequenceNumber" type="xs:string" minOccurs="0"/>
                                    <xs:element name="quantity" type="xs:string" minOccurs="0"/>
                                    <xs:element name="tallyLimit" type="xs:string" minOccurs="0"/>
                                    <xs:element name="bookingStatus" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Container Category may be one of the folllowing
                                                1. Full - (FCL)
                                                2. Empty - (MTY)
                                                3. Container Less than full Container Load - (LCL)
                                                4. BreakBulk - (BBK)
                                                5. Unknown
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="grossWeight" type="xs:string" minOccurs="0"/>
                                    <xs:element name="grossWtUnit" type="edi:massUnit" minOccurs="0"/>
                                    <xs:element name="safeWeight" type="xs:string" minOccurs="0"/>
                                    <xs:element name="safeWtUnit" type="edi:massUnit" minOccurs="0"/>
                                    <xs:element name="CSCExpiry" type="xs:string" minOccurs="0"/>
                                    <xs:element name="ISOcode" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>The ISO-code of the Booking Item
                                                The first digit denotes container length
                                                2 - 20ft
                                                4 - 40ft
                                                The second digit denotes height
                                                0,1 - 8ft high
                                                2,3 - 8ft 6inches high
                                                4,5 - over 8ft 6inches high (high cube)
                                                6,7 - 4ft 3inches high (half height)
                                                8 - 4ft 3 inches to 8ft high
                                                9 - less than 4ft high
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="temperatureRequired" type="xs:string" minOccurs="0"/>
                                    <xs:element name="temperatureRequiredUnit" type="xs:string" minOccurs="0"/>
                                    <xs:element name="remarks" type="xs:string" minOccurs="0"/>
                                    <xs:element name="ediReeferRqmnts" type="edi:reeferRqmnts" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Reefer details of a Booking Item - Co2, o2, humidity and vent settings.
                                                Controlled temperature suitable for chilled or frozen cargoes. It can be a porthole
                                                (must be fitted with or to refrigerating equipment) or an integral (has built-in refrigeration equipment).
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="ediCommodity" type="edi:ediCommodity" minOccurs="0"/>
                                    <xs:element name="dimension" type="edi:dimension" minOccurs="0"/>
                                    <xs:element name="ediHazard" type="edi:ediHazard" minOccurs="0" maxOccurs="unbounded"/>
                                    <xs:element name="gradeId" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Equipment Grade (must be configured Equipment Grade)</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="featureId" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Equipment FEATURE (must be configured Equipment Feature)</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="conditionId" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Equipment Condition (must be configured Equipment Conditions)</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                        <!-- ANSI message sometimes comes with equipment details irrespective of the booking item detail
                        Y4 has booking item details and N7 - Equipment details
                        -->
                        <xs:element name="ediBookingEquipment" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Sometimes the EDI messages come with full Equipment details irrespective of the booking item details
                                    in that case this element should be used.
                                </xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="bkgItemEquipmentSeqNbr" type="xs:string" minOccurs="0"/>
                                    <xs:element name="quantity" type="xs:string" minOccurs="0"/>
                                    <xs:element name="ediContainer" type="edi:ediContainer" minOccurs="0"/>
                                    <xs:element name="ediReeferRqmnts" type="edi:reeferRqmnts" minOccurs="0"/>
                                    <xs:element name="ediCommodity" type="edi:ediCommodity" minOccurs="0"/>
                                    <xs:element name="ediHazard" type="edi:ediHazard" minOccurs="0" maxOccurs="unbounded"/>
                                    <xs:element name="ediFlexFields" type="edi:ediFlexFields" minOccurs="0"/>
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                    <xs:attribute name="ediOrderPurpose" type="xs:string"/>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
</xs:schema>
