<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:edi="http://www.navis.com/argo" targetNamespace="http://www.navis.com/argo"
           elementFormDefault="qualified" attributeFormDefault="qualified">
    <xs:include schemaLocation="baseMapping.xsd"/>
    <xs:element name="railOrderTransactions">
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="edi:railOrderTransaction" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="railOrderTransaction">
        <xs:complexType>
            <xs:complexContent>
                <xs:extension base="edi:basetransaction">
                    <xs:sequence>
                        <xs:element name="ediTrainVisit" type="edi:ediTrainVisit" minOccurs="1" maxOccurs="1"/>
                        <xs:element name="shippingLine" type="edi:shippingLine" minOccurs="0"/>
                        <xs:element name="shipper" type="edi:shipper" minOccurs="0"/>
                        <xs:element name="consignee" type="edi:consignee" minOccurs="0"/>
                        <xs:element name="ediHazard" type="edi:ediHazard" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Hazard details associated with the rail order simply state that the containers arriving for
                                    this rail order will contain these hazards. This does not mean that all containers for this rail order will
                                    contain all of the hazard classes defined.
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                        <xs:element name="truckingCompany" type="edi:truckingCompany" minOccurs="0"/>
                        <xs:element name="loadPort" type="edi:port" minOccurs="0"/>
                        <xs:element name="dischargePort1" type="edi:port" minOccurs="0"/>
                        <xs:element name="dischargePort2" type="edi:port" minOccurs="0"/>
                        <xs:element name="optionalPort1" type="edi:port" minOccurs="0"/>
                        <xs:element name="optionalPort2" type="edi:port" minOccurs="0"/>
                        <xs:element name="optionalPort3" type="edi:port" minOccurs="0"/>
                        <xs:element name="Origin" type="edi:port" minOccurs="0"/>
                        <xs:element name="Destination" type="edi:port" minOccurs="0"/>
                        <xs:element name="ediRailOrderItem" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>Represents a rail order item</xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="quantity" type="xs:string" minOccurs="0"/>
                                    <xs:element name="grossWeight" type="xs:string" minOccurs="0"/>
                                    <xs:element name="grossWtUnit" type="edi:massUnit" minOccurs="0"/>
                                    <xs:element name="ISOcode" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>The ISO-code of the Booking Item
                                                The first digit denotes container length
                                                2 - 20ft
                                                4 - 40ft
                                                The second digit denotes height
                                                0,1 - 8ft high
                                                2,3 - 8ft 6inches high
                                                4,5 - over 8ft 6inches high (high cube)
                                                6,7 - 4ft 3inches high (half height)
                                                8 - 4ft 3 inches to 8ft high
                                                9 - less than 4ft high
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="temperatureRequired" type="xs:string" minOccurs="0"/>
                                    <xs:element name="temperatureRequiredUnit" type="xs:string" minOccurs="0"/>
                                    <xs:element name="ediReeferRqmnts" type="edi:reeferRqmnts" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Reefer details of a rail order Item - Co2, o2, humidity and vent settings.
                                                Controlled temperature suitable for chilled or frozen cargoes. It can be a porthole
                                                (must be fitted with or to refrigerating equipment) or an integral (has built-in refrigeration equipment).
                                            </xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="ediCommodity" type="edi:ediCommodity" minOccurs="0"/>
                                    <xs:element name="ediHazard" type="edi:ediHazard" minOccurs="0" maxOccurs="unbounded"/>
                                    <xs:element name="gradeId" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Equipment Grade (must be configured Equipment Grade)</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                    <xs:element name="featureId" type="xs:string" minOccurs="0">
                                        <xs:annotation>
                                            <xs:documentation>Equipment FEATURE (must be configured Equipment Feature)</xs:documentation>
                                        </xs:annotation>
                                    </xs:element>
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                        <xs:element name="ediRailOrderEquipment" minOccurs="0" maxOccurs="unbounded">
                            <xs:annotation>
                                <xs:documentation>EDI messages come with full Equipment details irrespective of the rail Order booking item details
                                    in that case this element should be used.
                                </xs:documentation>
                            </xs:annotation>
                            <xs:complexType>
                                <xs:sequence>
                                    <xs:element name="quantity" type="xs:string" minOccurs="0"/>
                                    <xs:element name="ediContainer" type="edi:ediContainer" minOccurs="1"/>
                                    <xs:element name="ediReeferRqmnts" type="edi:reeferRqmnts" minOccurs="0"/>
                                    <xs:element name="ediCommodity" type="edi:ediCommodity" minOccurs="0"/>
                                    <xs:element name="ediHazard" type="edi:ediHazard" minOccurs="0" maxOccurs="unbounded"/>
                                    <xs:element name="ediFlexFields" type="edi:ediFlexFields" minOccurs="0"/>
                                </xs:sequence>
                            </xs:complexType>
                        </xs:element>
                    </xs:sequence>
                    <xs:attribute name="railOrderNbr" type="xs:string" use="required"/>
                    <xs:attribute name="railOrderCategory">
                        <xs:annotation>
                            <xs:documentation>Rail Order Category may be one of the folllowing
                                1. Export   - (EXPRT)
                                2. Domestic - (DMSTC)
                            </xs:documentation>
                        </xs:annotation>
                        <xs:simpleType>
                            <xs:union>
                                <xs:simpleType>
                                    <xs:restriction base="xs:string">
                                        <xs:pattern value="((EXPRT)|(DMSTC))"/>
                                    </xs:restriction>
                                </xs:simpleType>
                                <xs:simpleType>
                                    <xs:restriction base="xs:string">
                                        <xs:pattern value=""/>
                                    </xs:restriction>
                                </xs:simpleType>
                            </xs:union>
                        </xs:simpleType>
                    </xs:attribute>
                    <xs:attribute name="freightKind" use="optional">
                        <xs:annotation>
                            <xs:documentation>RailOrder freight kind may be one of the following
                                1. Full - (FCL)
                                2. Empty - (MTY)
                                3. Container Less than full Container Load - (LCL)
                                4. BreakBulk - (BBK)
                            </xs:documentation>
                        </xs:annotation>
                        <xs:simpleType>
                            <xs:union>
                                <xs:simpleType>
                                    <xs:restriction base="xs:string">
                                        <xs:pattern value="((FCL)|(MTY)|(LCL)|(BBK))"/>
                                    </xs:restriction>
                                </xs:simpleType>
                                <xs:simpleType>
                                    <xs:restriction base="xs:string">
                                        <xs:pattern value=""/>
                                    </xs:restriction>
                                </xs:simpleType>
                            </xs:union>
                        </xs:simpleType>
                    </xs:attribute>
                    <xs:attribute name="facilityId" type="xs:string" use="optional"/>
                </xs:extension>
            </xs:complexContent>
        </xs:complexType>
    </xs:element>
</xs:schema>
